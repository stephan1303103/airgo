package com.sda.airgo.service;

import com.sda.airgo.datasource.Flights;
import com.sda.airgo.repository.FlightsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class FlightsService {

    @Autowired
    private FlightsRepository flightsRepository;


    public Flights create(Flights flights) {
        return flightsRepository.save(flights);

    }

    public List<Flights> findAll() {
        return flightsRepository.findAll();

    }

    public Flights findById(Integer id) {
        return flightsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Unable to find flight " + id));
    }

    public Flights update(Integer id, Flights flights) {
        Flights dbFlights = findById(id);
        dbFlights.setDepartureLocation(flights.getDepartureLocation());
        dbFlights.setDepartureDate(flights.getDepartureDate());
        dbFlights.setArrivalLocation(flights.getArrivalLocation());
        dbFlights.setArrivalDate(flights.getArrivalDate());
        dbFlights.setSeats(flights.getSeats());
        dbFlights.setCompany(flights.getCompany());
        dbFlights.setDepartureHour(flights.getDepartureHour());
        dbFlights.setArrivalHour(flights.getArrivalHour());
        dbFlights.setPrice(flights.getPrice());
        return flightsRepository.save(dbFlights);

    }

    public void delete(Integer id) {
        flightsRepository.deleteById(id);

    }

}
