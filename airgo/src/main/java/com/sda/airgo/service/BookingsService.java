package com.sda.airgo.service;

import com.sda.airgo.datasource.Bookings;
import com.sda.airgo.datasource.Flights;
import com.sda.airgo.datasource.User;
import com.sda.airgo.dto.BookingFlight;
import com.sda.airgo.exceptions.UserNotFoundException;
import com.sda.airgo.repository.BookingsRepository;
import com.sda.airgo.repository.FlightsRepository;
import com.sda.airgo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookingsService {

    @Autowired
    private BookingsRepository bookingsRepository;

    @Autowired
    private FlightsRepository flightRepository;

    @Autowired
    private UserRepository userRepository;



    public List<Bookings> findAll() {
        return bookingsRepository.findAll();

    }

    public Bookings findById(Integer id) {
        return bookingsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Unable to find booking " + id));
    }


    public Bookings bookFlight(BookingFlight bookingFlight) throws UserNotFoundException, Exception {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        Optional<Flights> flights = flightRepository.findById(bookingFlight.getFlightId());
        Flights flight = flights.get();

        if (bookingFlight.getBookedSeats() <= flight.getSeats() && bookingFlight.getBookedSeats() != 0) {

            User user = userRepository.findByEmail(userDetails.getUsername());
            Date date = new Date();
            Bookings entity = new Bookings();
            entity.setUserId(user.getId());
            entity.setFlightId(bookingFlight.getFlightId());
            entity.setBookedSeats(bookingFlight.getBookedSeats());
            flight.setSeats(flight.getSeats()- bookingFlight.getBookedSeats());
            entity.setBookingDate(new Date());

            if (user == null) {
            throw new UserNotFoundException();
            }

            return bookingsRepository.save(entity);

        } else throw new Exception("unavailable flight");

    }

    public void cancelBooking(Integer id){

        Optional<Bookings> bookings = bookingsRepository.findById(id);
        Bookings booking = bookings.get();

        Optional<Flights> flights = flightRepository.findById(booking.getFlightId());
        Flights flight = flights.get();

        flight.setSeats(flight.getSeats() + booking.getBookedSeats());
        flightRepository.save(flight);
        bookingsRepository.deleteById(id);
    }


}