package com.sda.airgo.service;

import com.sda.airgo.datasource.PasswordRecovery;
import com.sda.airgo.datasource.Role;
import com.sda.airgo.datasource.User;
import com.sda.airgo.dto.ResetPassword;
import com.sda.airgo.exceptions.FiledIsMandatoryException;
import com.sda.airgo.exceptions.UserNotFoundException;
import com.sda.airgo.repository.PasswordRecoveryRepository;
import com.sda.airgo.repository.RoleRepository;
import com.sda.airgo.repository.UserRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordRecoveryRepository passwordRecoveryRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User create(User user) {
        validate(user);

        long count = userRepository.countByEmail(user.getEmail());
        if (count > 0) {
            throw new RuntimeException("Email already in used");
        }

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

    public User findById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Unable to find user with id: " + id));
    }

    public User update(Integer id, User user) {
        User dBuser = findById(id);
        dBuser.setFirstName(user.getFirstName());
        dBuser.setLastName(user.getLastName());
        dBuser.setEmail(user.getEmail());
        dBuser.setAddress(user.getAddress());
        dBuser.setPassword(user.getPassword());
        dBuser.setPhone(user.getPhone());
        return userRepository.save(dBuser);
    }

    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void validate(User user) {
        if (Strings.isEmpty(user.getFirstName())) {
            throw new FiledIsMandatoryException("First name is mandatory");
        }

        if (Strings.isEmpty(user.getLastName())) {
            throw new FiledIsMandatoryException("Last name is mandatory");
        }

        if (Strings.isEmpty(user.getPhone())) {
            throw new FiledIsMandatoryException("Phone number is mandatory");
        }

        if (Strings.isEmpty(user.getEmail())) {
            throw new FiledIsMandatoryException("Email is mandatory");
        }

    }

    public void promote(Integer id) {
        User dbUser = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException());

        Role role = new Role();
        role.setUserId(dbUser.getId());
        role.setRole("ADMIN");
        roleRepository.save(role);

    }

    public String forgetPassword(String email) {
        User user = userRepository.findByEmail(email);

        if (user == null) {
            throw new RuntimeException("User not found");
        }

        String uuid = UUID.randomUUID().toString();
        PasswordRecovery passwordRecovery = new PasswordRecovery();
        passwordRecovery.setDate(new Date());
        passwordRecovery.setUserId(user.getId());
        passwordRecovery.setUid(uuid);

        passwordRecoveryRepository.save(passwordRecovery);
        return uuid;

    }

    public void resetPassword(ResetPassword resetPassword) {

        PasswordRecovery passwordRecovery = passwordRecoveryRepository.findByUid(resetPassword.getUid());
        if (passwordRecovery == null) {
            throw new RuntimeException("BAD REQUEST");
        }
        User user = userRepository.findById(passwordRecovery.getUserId()).get();
        if (user == null) {
            throw new RuntimeException("BAD REQUEST");
        }

        user.setPassword(bCryptPasswordEncoder.encode(resetPassword.getNewPassword()));
        user.setPassword(resetPassword.getNewPassword());
        userRepository.save(user);
        passwordRecoveryRepository.delete(passwordRecovery);

    }


}
