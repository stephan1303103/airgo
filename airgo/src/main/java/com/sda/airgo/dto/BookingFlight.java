package com.sda.airgo.dto;

public class BookingFlight {
    private Integer flightId;
    private Integer bookedSeats;

        public Integer getFlightId() {
                return flightId;
        }

        public void setFlightId(Integer flightId) {
                this.flightId = flightId;
        }

        public Integer getBookedSeats() {
                return bookedSeats;
        }

        public void setBookedSeats(Integer bookedSeats) {
                this.bookedSeats = bookedSeats;
        }


}
