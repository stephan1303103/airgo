package com.sda.airgo.repository;

import com.sda.airgo.datasource.PasswordRecovery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordRecoveryRepository extends JpaRepository<PasswordRecovery,Integer> {
    PasswordRecovery findByUid(String uId);
}
