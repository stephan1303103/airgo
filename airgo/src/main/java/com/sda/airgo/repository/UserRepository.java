package com.sda.airgo.repository;


import com.sda.airgo.datasource.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByEmail(String email);

    long  countByEmail(String email);

}
