package com.sda.airgo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus (code = HttpStatus.BAD_REQUEST,
        reason = "The flight is either cancelled or fully booked. Please check the flights' schedule.")
public class FlightUnavailableException extends RuntimeException{
}
