package com.sda.airgo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Your booking PNR is either incorrect or nonexistent!")
public class BookingNotFoundException extends RuntimeException{
}
