package com.sda.airgo.controller;

import com.sda.airgo.datasource.Bookings;
import com.sda.airgo.dto.BookingFlight;
import com.sda.airgo.service.BookingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bookings")
public class BookingsController {

    @Autowired
    private BookingsService bookingsService;


    @Secured("ROLE_ADMIN")
    @GetMapping("/findAll")
    public List<Bookings> findAll() {
        return bookingsService.findAll();
    }

    @GetMapping("/{id}")
    public Bookings findById(@PathVariable(name ="id")Integer id){
        return bookingsService.findById(id);
    }


    @PostMapping()
    public Bookings bookFlight
    (@RequestBody BookingFlight bookingFlight) throws Exception {
            return bookingsService.bookFlight(bookingFlight);

    }

    @DeleteMapping ("/{id}")
    public void cancelBooking(@PathVariable(name = "id") Integer id) {
        bookingsService.cancelBooking(id);
    }

}
