package com.sda.airgo.controller;

import com.sda.airgo.datasource.User;
import com.sda.airgo.dto.ResetPassword;
import com.sda.airgo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public User create(@RequestBody User user) {
        return userService.create(user);
    }


    @PutMapping("/{id}")
    public User update(
            @PathVariable(name = "id") Integer id,
            @RequestBody User user) {
        return userService.update(id, user);
    }


    @GetMapping("/findAll")
    public List<User> findAll(){
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable(name="id") Integer id){
        return userService.findById(id);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping ("/{id}")
    public void delete(@PathVariable(name = "id") Integer id){
        userService.delete(id);
    }

    @PostMapping("/forgetPassword/{email}")
    public String forgetPassword(@PathVariable(name = "email") String email) {
        return userService.forgetPassword(email);
    }

    @PostMapping("/resetPassword")
    private void resetPassword(@RequestBody ResetPassword resetPassword) {
        userService.resetPassword(resetPassword);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/promote/{id}")
    public void promote(
            @PathVariable(name = "id") Integer id) {
        userService.promote(id);
    }

}
