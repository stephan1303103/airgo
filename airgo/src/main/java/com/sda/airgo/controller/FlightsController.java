package com.sda.airgo.controller;


import com.sda.airgo.datasource.Flights;
import com.sda.airgo.service.FlightsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/flight")
public class FlightsController {

    @Autowired
    private FlightsService flightsService;

    @Secured("ROLE_ADMIN")
    @PostMapping
    public Flights create(@RequestBody Flights flights) {
        return flightsService.create(flights);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    public Flights update(
            @PathVariable(name = "id") Integer id,
            @RequestBody Flights flights){
        return flightsService.update(id, flights);
}
    @Secured("ROLE_ADMIN")
    @DeleteMapping ("/{id}")
    public void delete(@PathVariable(name = "id")Integer id){
        flightsService.delete(id);
    }

    @GetMapping()
    public List<Flights> findAll() {
        return flightsService.findAll();
    }

    @GetMapping("/{id}")
    public Flights findById(@PathVariable(name ="id")Integer id){
        return flightsService.findById(id);
    }



}
